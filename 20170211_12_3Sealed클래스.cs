﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastCampus_test
{

    //클래스 선언
    public  sealed class Employee
    {
        public int BithYear;      //필드선언
        public string Name;       //필드선언  
        public Employee() { }           //기본생성자 선언
        public Employee(int bithYear, string name)   //생성자 오버로딩
        {
            this.BithYear = bithYear;             //초기화 코드
            this.Name = name;
                        
        }
     }
     


    class Program
    {

        static void Main(string[] args)
        {

            Employee emp = new Employee();
            emp.BithYear = 1988;
            emp.Name = "김형진";

            Console.WriteLine("BirthYear:{0},Name:{1}", emp.BithYear,emp.Name);
        }

    }
}