﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastCampus_test
{

    //클래스 선언
    public class Employee
    {
        public int bithYear;
        public string name;



        public int BirthYear
        {
            get
            {
                return this.birthYear;
            }
            set {
                if (value < 1900)
                {
                    throw new ArgumentNullException("1900년 이상만 지정이 가능합니다.");
                }

                this.birthYear = value;

                }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set {
                this.name = value;
            }
        }

     }



    class Program
    {

        static void Main(string[] args)
        { try
            {

                Employee emp = new Employee();
                emp.BithYear = 1988;
                emp.Name = "김형진";

                Console.WriteLine("BirthYear:{0},Name:{1}", emp.BithYear, emp.Name);

            }

        catch (ArgumentException ex){
                Console.WriteLine("예외발생: {0}", ex.Message);
            }            
        }
    }
}