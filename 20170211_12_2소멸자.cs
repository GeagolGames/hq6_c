﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastCampus_test
{


   //생성자와 소멸자의 각 시점 코드

    public class InitFinalizer : IDisposable
    {
public InitFinalizer()
        {
            Console.WriteLine("생성자");
        }

        ~InitFinalizer()
        {
            Console.WriteLine("소멸자");
        }
        public void Dispose()
        {
            Console.WriteLine("Diospose");
        }

     }
     


    class Program
    {

        static void Main(string[] args)
        {
            //호출
            InitFinalizer initTest = new InitFinalizer();
            initTest.Dispose();
        }

    }
}