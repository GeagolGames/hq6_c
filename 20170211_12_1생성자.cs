﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FastCampus_test
{

    //클래스 선언
    public class Employee
    {
        public int BithYear;
        public string Name;
        public Employee() { }
        public Employee(int bithYear, string name)
        {
            this.BithYear = bithYear;
            this.Name = name;
                        
        }
     }
     


    class Program
    {

        static void Main(string[] args)
        {

            Employee emp = new Employee();
            emp.BithYear = 1988;
            emp.Name = "김형진";

            Console.WriteLine("BirthYear:{0},Name:{1}", emp.BithYear,emp.Name);

            //Object Initializer
            Employee emp2 = new Employee
            {
                BithYear = 1888,
                Name = "홍길동"
            };
            Console.WriteLine("BirthYear:{0},Name:{1}", emp2.BithYear, emp2.Name)
        }

    }
}